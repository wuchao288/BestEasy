﻿using System;
using System.Collections.Generic;

using BestEasy.Core;
using BestEasy.Core.UnitOfWork;
using BestEasy.NPoco;

using NPoco.Linq;
using System.Threading.Tasks;

namespace BestEasy.Data.NPoco
{
    public class NPocoRepositoryBase<T, TId> : IUnitOfWorkRepository where T : IAggregateRoot
    {
        #region 属性
        private readonly IDbBase _dbBase;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region 构造函数
        public NPocoRepositoryBase(IUnitOfWork unitOfWork)
        {
            _dbBase = NPocoDbHelper.CurrentDb;
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region UnitOfWork

        public void Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            _unitOfWork.RegisterAdd(entity, this);
        }
        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            _unitOfWork.RegisterUpdate(entity, this);
        }
        public void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            _unitOfWork.RegisterDelete(entity, this);
        }
        public void PersistCreationOf(IAggregateRoot entity)
        {
            _dbBase.Insert(entity);
        }
        public void PersistUpdateOf(IAggregateRoot entity)
        {
            _dbBase.Update(entity);
        }
        public void PersistDeletionOf(IAggregateRoot entity)
        {
            _dbBase.Delete(entity);
        }
        #endregion

        #region Delete Inser Update

        public virtual async Task<object> InsertAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            return await _dbBase.InsertAsync(entity);
        }

        public async Task<int> UpdateAsync(object poco)
        {
            if (poco == null)
                throw new ArgumentNullException(nameof(poco));
            return await _dbBase.UpdateAsync(poco);
        }

        public async Task<int> DeleteAsync(object poco)
        {
            if (poco == null)
                throw new ArgumentNullException(nameof(poco));
            return await _dbBase.DeleteAsync(poco);
        }

        public IDeleteQueryProvider<T> DeleteMany()
        {
            return _dbBase.DeleteMany<T>();
        }
        public IUpdateQueryProvider<T> UpdateMany()
        {
            return _dbBase.UpdateMany<T>();
        }
        public virtual int Delete(TId id)
        {
            return _dbBase.Delete<T>(id);
        }
        public virtual void InsertBulk(IEnumerable<T> entitys)
        {
            if (entitys == null)
                throw new ArgumentNullException(nameof(entitys));

            using (var scope = _dbBase.GetTransaction())
            {
                foreach (var item in entitys)
                {
                    _dbBase.Insert(item);
                }
                scope.Complete();
            }
        }
        public virtual void UpdateBulk(IEnumerable<T> entitys)
        {
            if (entitys == null)
                throw new ArgumentNullException(nameof(entitys));
            using (var scope = _dbBase.GetTransaction())
            {
                foreach (var item in entitys)
                {
                    _dbBase.Update(item);
                }
                scope.Complete();
            }
        }
        #endregion

        #region Query
        public virtual T GetById(TId id)
        {
            return _dbBase.SingleOrDefaultById<T>(id);
        }

        public virtual IQueryProviderWithIncludes<T> Query()
        {
            return _dbBase.Query<T>();
        }
        #endregion

        #region Other
        public virtual bool Exists(TId id)
        {
            return _dbBase.Exists<T>(id);
        }
        public virtual bool IsNew(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            return _dbBase.IsNew(entity);
        }
        #endregion


    }
}
