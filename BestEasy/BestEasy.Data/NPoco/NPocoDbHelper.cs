﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BestEasy.NPoco;

using NPoco;

namespace BestEasy.Data.NPoco
{
    /// <summary>
    /// 数据库帮助类
    /// </summary>
    public class NPocoDbHelper
    {
        private static IDbBase _dbBase;

        private static void Init()
        {
            _dbBase = new DbBase();
        }
        /// <summary>
        /// 默认数据库配置实例
        /// </summary>
        public static IDbBase CurrentDb
        {
            get
            {
                if (_dbBase == null)
                    Init();
                return _dbBase;
            }
        }
        /// <summary>
        /// 新数据库实例
        /// </summary>
        /// <param name="connectionStringName">配置文件连接对象名称</param>
        /// <returns></returns>
        public static IDatabase GetNewDb(string connectionStringName)
        {
            return new DbHelper(connectionStringName).Database;
        }
    }
}
