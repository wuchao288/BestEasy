﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BestEasy.Core;
using System.Data.Entity;

namespace BestEasy.Data.Ef
{
    public interface IDbContext
    {
        /// <summary>
        /// Get DbSet
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <returns>DbSet</returns>
        IDbSet<TEntity> Set<TEntity>() where TEntity : EfEntityBase;

        /// <summary>
        /// 保存修改
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// 执行存储过程并在结束时加载实体列表
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <param name="commandText">存储过程名</param>
        /// <param name="parameters">参数</param>
        /// <returns>实体列表</returns>
        IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters)
            where TEntity : EfEntityBase, new();

        /// <summary>
        /// 创建一个原始的SQL查询将返回给定的泛型类型的元素。
        /// 类型可以是任何类型，具有匹配的列的名称，返回的查询，或可以是一个简单的原始类型。
        /// 该类型不必须是一个实体类型。
        /// 此查询的结果不被上下文跟踪，即使返回的对象类型是一个实体类型。
        /// </summary>
        /// <typeparam name="TElement">返回的类型的对象的查询。</typeparam>
        /// <param name="sql">SQL查询字符串。</param>
        /// <param name="parameters">参数</param>
        /// <returns>结果集</returns>
        IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="sql">SQL查询语句</param>
        /// <param name="doNotEnsureTransaction"></param>
        /// <param name="timeout"></param>
        /// <param name="parameters">参数</param>
        /// <returns>执行命令后返回的数据库的结果.</returns>
        int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters);
    }
}
