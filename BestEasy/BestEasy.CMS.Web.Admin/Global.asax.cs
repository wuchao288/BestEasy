﻿using BestEasy.CMS.Services;
using BestEasy.Core.Caching;
using BestEasy.Core.Configuration;
using BestEasy.Core.Logging;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BestEasy.CMS.Web.Admin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BootStrapper.ConfigureDependencies();
            AutoMapperBootStrapper.ConfigureAutoMapper();

            ControllerBuilder.Current.SetControllerFactory(new IoCControllerFactory());

            ApplicationSettingsFactory.InitializeApplicationSettingsFactory(ObjectFactory.GetInstance<WebConfigApplicationSettings>());
            LoggingFactory.InitializeLogFactory(ObjectFactory.GetInstance<Log4NetAdapter>());

            CacheHelper.Start();//启动缓存
        }
    }
}
