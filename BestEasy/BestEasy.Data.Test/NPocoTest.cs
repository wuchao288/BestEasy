﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Configuration;
using System.Collections.Generic;

using NPoco;
using NPoco.Linq;

using BestEasy.Core;
using BestEasy.Core.Data;
using BestEasy.Core.Data.NPoco;
using BestEasy.Core.UnitOfWork;

using BestEasy.Data;
using BestEasy.Data.NPoco;

using BestEasy.NPoco;

using StructureMap;
using System.Linq.Expressions;

namespace BestEasy.Data.Test
{
    [TestClass]
    public class NPocoTest
    {
        //属性
        private readonly UserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        //构造函数
        public NPocoTest()
        {
            BootStrapper.ConfigureDependencies();
            //获取实例
            _unitOfWork = ObjectFactory.GetInstance<UnitOfWork>();
            _userRepository = new UserRepository(_unitOfWork);
        }
        //创建表
        [TestMethod]
        public void CreateTableTest()
        {
            var arr = DbHelper.SqlFileToArrayList(@"E:\360云盘自动备份\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\User.sql", "TestDb");
            //var arr = DbHelper.SqlFileToArrayList(@"E:\ZhengJinfan\云盘同步文件夹\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\User.sql", "TestDb");
            var result = DbHelper.ExecuteCommand(arr, ConfigurationManager.ConnectionStrings[0].ConnectionString);
            Assert.IsTrue(result);
        }
        //添加单条数据
        [TestMethod]
        public void InsertTest()
        {
            //创建表
            CreateTable();
            //初始化数据
            var user = new User
            {
                Name = "ZhangSan",
                CreateTime = DateTime.Now
            };
            _userRepository.Insert(user);
            _unitOfWork.Commit();//提交
            var result = _userRepository.GetById(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, 1);
            Assert.AreEqual(result.Name, "ZhangSan");
        }
        //添加多条数据
        [TestMethod]
        public void InserBulkTest()
        {
            //创建表
            CreateTable();
            var users = new List<User>
            {
                new User { Name="张三",CreateTime=DateTime.Now},
                new User { Name="李四",CreateTime=DateTime.Now},
                new User { Name="王五",CreateTime=DateTime.Now},
                new User { Name="赵六",CreateTime=DateTime.Now},
                new User { Name="王小丽",CreateTime=DateTime.Now}
            };
            _userRepository.InsertBulk(users);
            var results = _userRepository.Query().ToList();
            Assert.IsTrue(results.Count > 0);
            Assert.AreEqual("赵六", results[3].Name);
            Assert.AreNotEqual(6, results.Count);
        }
        //新增和修改
        [TestMethod]
        public void InsertAndUpdateTest()
        {
            //创建表
            CreateTable();
            //初始化数据
            var user = new User
            {
                Name = "ZhangSan",
                CreateTime = DateTime.Now
            };
            _userRepository.Insert(user);
            _unitOfWork.Commit();//提交
            var result = _userRepository.GetById(1);
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, 1);
            Assert.AreEqual(result.Name, "ZhangSan");

            var uow = new UnitOfWork();
            var repository = new UserRepository(uow);
            result.Name = "张三";
            repository.Update(result);
            uow.Commit();
            Assert.AreEqual("张三", _userRepository.GetById(1).Name);
        }
        //批量新增和批量修改
        [TestMethod]
        public void InsertBulkAndUpdateBulkTest()
        {
            //创建表
            CreateTable();
            var users = new List<User>
            {
                new User { Name="张三",CreateTime=DateTime.Now},
                new User { Name="李四",CreateTime=DateTime.Now},
                new User { Name="王五",CreateTime=DateTime.Now},
                new User { Name="赵六",CreateTime=DateTime.Now},
                new User { Name="王小丽",CreateTime=DateTime.Now}
            };
            _userRepository.InsertBulk(users);
            var results = _userRepository.Query().ToList();

            Assert.AreEqual("张三", results[0].Name);
            Assert.AreEqual("赵六", results[3].Name);

            results[0].Name = "ZhangSan";
            results[3].Name = "ZhaoLiu";

            _userRepository.UpdateBulk(results);
            var uResults = _userRepository.Query().ToList();

            Assert.AreEqual("ZhangSan", uResults[0].Name);
            Assert.AreEqual("ZhaoLiu", uResults[3].Name);
        }
        [TestMethod]
        //是否为新数据
        public void IsNewTest()
        {
            //创建表
            CreateTable();
            //初始化数据
            var user = new User
            {
                Name = "ZhangSan",
                CreateTime = DateTime.Now
            };
            _userRepository.Insert(user);
            _unitOfWork.Commit();//提交
            var result = _userRepository.GetById(1);
            Assert.AreEqual(result.Id, 1);

            var flag1 = _userRepository.IsNew(result);
            Assert.IsFalse(flag1);

            var user1 = new User { Name = "LiSi", CreateTime = DateTime.Now };
            var flag2 = _userRepository.IsNew(user1);
            Assert.IsTrue(flag2);
        }
        //数据是否存在
        [TestMethod]
        public void ExistsTest()
        {
            //创建表
            CreateTable();
            //初始化数据
            var user = new User
            {
                Name = "ZhangSan",
                CreateTime = DateTime.Now
            };
            _userRepository.Insert(user);
            _unitOfWork.Commit();//提交
            var flag = _userRepository.Exists(1);
            Assert.IsTrue(flag);

            var flag1 = _userRepository.Exists(5);
            Assert.IsFalse(flag1);


        }
        [TestMethod]
        //
        public void UpdateMaryTest()
        {
            CreateTable();
            var users = new List<User>
            {
                new User { Name="张三",CreateTime=DateTime.Now},

                new User { Name="李四",CreateTime=DateTime.Now},
                new User { Name="王五",CreateTime=DateTime.Now},
                new User { Name="赵六",CreateTime=DateTime.Now},
                new User { Name="王小丽",CreateTime=DateTime.Now}
            };
            _userRepository.InsertBulk(users);
            var results = _userRepository.Query().ToList();
            Assert.AreEqual("赵六", results[3].Name);

            var uUser = results[4];
            uUser.Name = "赵七";
            uUser.CreateTime = DateTime.Now.AddDays(5);
            _userRepository.UpdateMany().Where(p => p.Name.Equals("赵六")).OnlyFields(p => new { p.Name, p.CreateTime }).Execute(uUser);
            var get = _userRepository.GetById(4);
            Assert.AreEqual("赵七", get.Name);
        }

        //创建表
        private static void CreateTable()
        {
            var arr = DbHelper.SqlFileToArrayList(@"E:\360云盘自动备份\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\User.sql", "TestDb");
            //var arr = DbHelper.SqlFileToArrayList(@"E:\ZhengJinfan\云盘同步文件夹\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\User.sql", "TestDb");
            var result = DbHelper.ExecuteCommand(arr, ConfigurationManager.ConnectionStrings[0].ConnectionString);
        }
        [TestMethod]
        public void CreateDataBaseAndTableTest()
        {
            var arr = DbHelper.SqlFileToArrayList(@"E:\360云盘自动备份\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\BestEasy.CMS.sql", "BestEasyCMS_DB");
            //var arr = DbHelper.SqlFileToArrayList(@"E:\ZhengJinfan\云盘同步文件夹\自动同步\Projects\BestEasy\BestEasy\BestEasy.Data.Test\BestEasy.CMS.sql", "BestEasyCMS_DB");
            var result = DbHelper.ExecuteCommand(arr, ConfigurationManager.ConnectionStrings[0].ConnectionString);
            Assert.IsTrue(result);
        }
    }

    /// <summary>
    /// 用户资源类
    /// </summary>
    public class UserRepository : NPocoRepositoryBase<User, long>, IRepository<User, long>, INPocoRepository<User, long>
    {
        public UserRepository(IUnitOfWork unitOfWork) :
            base(unitOfWork)
        {

        }

    }
    //用户实体
    [TableName("tb_Users")]
    [PrimaryKey("Id")]
    public class User : EntityBase<long>
    {
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
