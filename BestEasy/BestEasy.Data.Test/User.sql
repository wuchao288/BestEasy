﻿if exists (select 1
            from  sysobjects
           where  id = object_id('tb_Users')
            and   type = 'U')
   drop table tb_Users
go

create table tb_Users (
   Id                   bigint               identity,
   Name             varchar(50)          null,
   CreateTime           datetime             null
   constraint PK_TB_USERS primary key nonclustered (Id)
)
go
