﻿using StructureMap;
using StructureMap.Configuration.DSL;

using BestEasy.NPoco;
using BestEasy.Core.UnitOfWork;

namespace BestEasy.Data.Test
{
    public class BootStrapper
    {
        /// <summary>
        /// 配置依赖
        /// </summary>
        public static void ConfigureDependencies()
        {
            ObjectFactory.Initialize(x => x.AddRegistry<ControllerRegistry>());
        }
        private class ControllerRegistry : Registry
        {
            public ControllerRegistry()
            {
                ForRequestedType<IDbBase>().TheDefault.Is.OfConcreteType<DbBase>();
                ForRequestedType<IUnitOfWork>().TheDefault.Is.OfConcreteType<UnitOfWork>();
            }
        }
    }    
}
