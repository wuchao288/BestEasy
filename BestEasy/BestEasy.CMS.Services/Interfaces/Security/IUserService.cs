﻿using BestEasy.CMS.Services.Messaging.Response;
using BestEasy.CMS.Services.Messaging.Request.Security;
using BestEasy.CMS.Services.ViewModels.Security;
using BestEasy.Core.Pageing;
using BestEasy.CMS.Services.Messaging.Response.Security;

using System.Collections.Generic;

namespace BestEasy.CMS.Services.Interfaces.Security
{
    /// <summary>
    /// 后台用户服务接口
    /// </summary>
    public interface IUserService : IService<UserView, long>
    {
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="views"></param>
        /// <returns></returns>
        ResponseBase InsertBulkUser(IEnumerable<UserView> views);
        /// <summary>
        /// 删除[逻辑]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResponseBase TakeDelete(long id);
        /// <summary>
        /// 获取用户信息[分页]
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetPageResponse<UserView> GetUsers(GetUserPageRequest request);
        /// <summary>
        /// 获取回收站里的用户信息[分页]
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        GetPageResponse<UserView> GetUsersByRecycleBin(Paged page);
        /// <summary>
        /// 获取回收站里的用户信息
        /// </summary>
        /// <returns></returns>
        GetListResponse<UserView> GetUsersByRecycleBin();
        /// <summary>
        /// 后台用户登录
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        LoginResponse Login(LoginRequest request);
        /// <summary>
        /// 登录名是否存在
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        bool LoginNameExists(string loginName);
    }
}
