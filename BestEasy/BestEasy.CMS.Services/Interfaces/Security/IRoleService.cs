﻿using BestEasy.CMS.Services.Messaging.Request.Security;
using BestEasy.CMS.Services.Messaging.Response;
using BestEasy.CMS.Services.ViewModels.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.Interfaces.Security
{
    public interface IRoleService : IService<RoleView, long>
    {
        ResponseBase Save(SaveRoleRequest request);
        /// <summary>
        /// 删除[逻辑]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResponseBase TakeDelete(long id);
        /// <summary>
        /// 获取角色信息[分页]
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        GetPageResponse<RoleView> GetRoles(GetRolePageRequest request);

        bool RoleNameExists(RoleNameExistsRequest request);
    }
}
