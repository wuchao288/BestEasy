﻿using BestEasy.Core.Pageing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.Messaging.Request.Security
{
    public class GetRolePageRequest
    {
        public GetRolePageRequest(int pageIndex, int pageSize)
        {
            Paged = new Paged(pageIndex, pageSize);
        }

        public Paged Paged { get; private set; }
    }
}
