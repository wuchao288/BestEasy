﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BestEasy.Core.Pageing;

namespace BestEasy.CMS.Services.Messaging.Request.Security
{
    public class GetUserPageRequest
    {
        public GetUserPageRequest(int pageIndex, int pageSize)
        {
            Paged = new Paged(pageIndex, pageSize);
        }

        public Paged Paged { get; private set; }

        public string LoginName { get; set; }
        public long? RoleId { get; set; }
        public bool? IsEnable { get; set; }
    }
}
