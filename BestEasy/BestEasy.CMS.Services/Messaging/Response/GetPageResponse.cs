﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace BestEasy.CMS.Services.Messaging.Response
{
    public class GetPageResponse<TView> : ResponseBase
    {
        public Page<TView> Pages { get; set; }
    }
}
