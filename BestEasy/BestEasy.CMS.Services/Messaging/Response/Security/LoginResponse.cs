﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.Messaging.Response.Security
{
    public class LoginResponse : ResponseBase
    {
        public LoginStatus Status { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public string UserName { get; set; }
    }
}
