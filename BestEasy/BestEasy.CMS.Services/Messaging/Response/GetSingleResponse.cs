﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.Messaging.Response
{
    public class GetSingleResponse<TView> : ResponseBase
    {
        public TView View { get; set; }
    }
}
