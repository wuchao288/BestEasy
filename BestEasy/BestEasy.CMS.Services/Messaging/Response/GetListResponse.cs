﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.Messaging.Response
{
    public class GetListResponse<TView> : ResponseBase
    {
        public IList<TView> Views { get; set; }
    }
}
