﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using BestEasy.CMS.Services.ViewModels.Security;
using BestEasy.CMS.Models.Security;

namespace BestEasy.CMS.Services
{
    public class AutoMapperBootStrapper
    {
        public static void ConfigureAutoMapper()
        {
            #region Security
            Mapper.CreateMap<User, UserView>().ForMember(p => p.RoleView, opt => opt.MapFrom(a => a.Role));
            Mapper.CreateMap<UserView, User>().ForMember(p => p.Role, opt => opt.MapFrom(a => a.RoleView));

            Mapper.CreateMap<Role, RoleView>();
            Mapper.CreateMap<RoleView, Role>();

            Mapper.CreateMap<Resource, ResourceView>();
            Mapper.CreateMap<ResourceView, Resource>();

            Mapper.CreateMap<ResourceInRole, ResourceInRoleView>()
                .ForMember(p => p.RoleView, opt => opt.MapFrom(a => a.Role))
                .ForMember(p => p.ResourceView, opt => opt.MapFrom(a => a.Resource));
            Mapper.CreateMap<ResourceInRoleView, ResourceInRole>()
                .ForMember(p => p.Role, opt => opt.MapFrom(a => a.RoleView))
                .ForMember(p => p.Resource, opt => opt.MapFrom(a => a.ResourceView));

            Mapper.CreateMap<ActionType, ActionTypeView>();
            Mapper.CreateMap<ActionTypeView, ActionType>();

            Mapper.CreateMap<RoleWithControllerAction, RoleWithControllerActionView>();
            Mapper.CreateMap<RoleWithControllerActionView, RoleWithControllerAction>();
            #endregion
        }
    }
}
