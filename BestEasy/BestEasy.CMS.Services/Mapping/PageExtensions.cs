﻿using System.Collections.Generic;
using NPoco;

namespace BestEasy.CMS.Services.Mapping
{
    public static class PageExtensions
    {
        public static Page<TView> ToPageView<TEntity, TView>(this Page<TEntity> pageEntity)
        {
            var pageView = new Page<TView>
            {
                CurrentPage = pageEntity.CurrentPage,
                TotalItems = pageEntity.TotalItems,
                TotalPages = pageEntity.TotalPages,
                ItemsPerPage = pageEntity.ItemsPerPage,
                Items = (List<TView>)pageEntity.Items.ToViews<TEntity, TView>()
            };
            return pageView;
        }
    }
}
