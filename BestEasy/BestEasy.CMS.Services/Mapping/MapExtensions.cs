﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;

namespace BestEasy.CMS.Services.Mapping
{
    public static class MapExtensions
    {
        public static TView ToView<TEntity, TView>(this TEntity entity)
        {
            return Mapper.Map<TEntity, TView>(entity);
        }
        public static TEntity ToEntity<TEntity, TView>(this TView view)
        {
            return Mapper.Map<TView, TEntity>(view);
        }

        public static IEnumerable<TView> ToViews<TEntity, TView>(this IEnumerable<TEntity> entitys)
        {
            var result = new List<TView>();
            foreach (var item in entitys)
            {
                result.Add(item.ToView<TEntity, TView>());
            }
            return result;
        }

        public static IEnumerable<TEntity> ToEntitys<TEntity, TView>(this IEnumerable<TView> views)
        {
            var result = new List<TEntity>();
            foreach (var item in views)
            {
                result.Add(item.ToEntity<TEntity, TView>());
            }
            return result;
        }

        public static IList<TView> ToViews<TEntity, TView>(this IList<TEntity> entitys)
        {
            var result = new List<TView>();
            foreach (var item in entitys)
            {
                result.Add(item.ToView<TEntity, TView>());
            }
            return result;
        }

        public static IList<TEntity> ToEntitys<TEntity, TView>(this IList<TView> views)
        {
            var result = new List<TEntity>();
            foreach (var item in views)
            {
                result.Add(item.ToEntity<TEntity, TView>());
            }
            return result;
        }
    }
}
