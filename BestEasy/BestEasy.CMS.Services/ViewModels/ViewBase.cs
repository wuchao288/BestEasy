﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.ViewModels
{
    public class ViewBase<TId>
    {
        public TId Id { get; set; }
    }
}
