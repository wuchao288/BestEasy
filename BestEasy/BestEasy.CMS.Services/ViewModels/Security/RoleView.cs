﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.CMS.Services.ViewModels.Security
{
    public class RoleView : ViewBase<long>
    {
        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 父路径 示：[0,3,8]
        /// </summary>
        public string ParentPath { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 是否为系统角色
        /// </summary>
        public bool IsSystemRole { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDel { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnable { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
