﻿/********************************************************************************
** 作者： Mick
** 邮箱： zheng_jinfan@126.com
** 主页： http://www.zhengjinfan.cn
** 创始时间：2016-3-25
** 描述：
**      数据库基类接口实现类，…
*********************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NPoco;
using NPoco.Linq;

namespace BestEasy.NPoco
{
    public class DbBase : IDbBase
    {
        private readonly IDatabase _dbBase;
        public DbBase() : this(DbHelper.CurrentDb)
        {
        }
        public DbBase(IDatabase dbBase)
        {
            _dbBase = dbBase;
        }

        #region Basic
        public void OpenSharedConnection()
        {
            _dbBase.OpenSharedConnection();
        }

        public void CloseSharedConnection()
        {
            _dbBase.CloseSharedConnection();
        }

        public int Execute(string sql, params object[] args)
        {
            return _dbBase.Execute(sql, args);
        }

        public int Execute(Sql sql)
        {
            return _dbBase.Execute(sql);
        }

        public async Task<int> ExecuteAsync(string sql, params object[] args)
        {
            return await _dbBase.ExecuteAsync(sql, args);
        }

        public async Task<int> ExecuteAsync(Sql sql)
        {
            return await _dbBase.ExecuteAsync(sql);
        }

        public T ExecuteScalar<T>(string sql, params object[] args)
        {
            return _dbBase.ExecuteScalar<T>(sql, args);
        }

        public T ExecuteScalar<T>(Sql sql)
        {
            return _dbBase.ExecuteScalar<T>(sql);
        }

        public async Task<T> ExecuteScalarAsync<T>(string sql, params object[] args)
        {
            return await _dbBase.ExecuteScalarAsync<T>(sql, args);
        }

        public async Task<T> ExecuteScalarAsync<T>(Sql sql)
        {
            return await _dbBase.ExecuteScalarAsync<T>(sql);
        }

        public List<T> Fetch<T>()
        {
            return _dbBase.Fetch<T>();
        }

        public List<T> Fetch<T>(string sql, params object[] args)
        {
            return _dbBase.Fetch<T>(sql, args);
        }

        public List<T> Fetch<T>(Sql sql)
        {
            return _dbBase.Fetch<T>(sql);
        }

        public async Task<List<T>> FetchAsync<T>(string sql, params object[] args)
        {
            return await _dbBase.FetchAsync<T>(sql, args);
        }

        public async Task<List<T>> FetchAsync<T>(Sql sql)
        {
            return await _dbBase.FetchAsync<T>(sql);
        }

        public List<T> Fetch<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return _dbBase.Fetch<T>(page, itemsPerPage, sql, args);
        }

        public List<T> Fetch<T>(long page, long itemsPerPage, Sql sql)
        {
            return _dbBase.Fetch<T>(page, itemsPerPage, sql);
        }

        public async Task<List<T>> FetchAsync<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return await _dbBase.FetchAsync<T>(page, itemsPerPage, sql, args);
        }

        public async Task<List<T>> FetchAsync<T>(long page, long itemsPerPage, Sql sql)
        {
            return await _dbBase.FetchAsync<T>(page, itemsPerPage, sql);
        }

        public Page<T> Page<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return _dbBase.Page<T>(page, itemsPerPage, sql, args);
        }

        public Page<T> Page<T>(long page, long itemsPerPage, Sql sql)
        {
            return _dbBase.Page<T>(page, itemsPerPage, sql);
        }

        public async Task<Page<T>> PageAsync<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return await _dbBase.PageAsync<T>(page, itemsPerPage, sql, args);
        }

        public async Task<Page<T>> PageAsync<T>(long page, long itemsPerPage, Sql sql)
        {
            return await _dbBase.PageAsync<T>(page, itemsPerPage, sql);
        }

        public List<T> SkipTake<T>(long skip, long take, string sql, params object[] args)
        {
            return _dbBase.SkipTake<T>(skip, take, sql, args);
        }

        public List<T> SkipTake<T>(long skip, long take, Sql sql)
        {
            return _dbBase.SkipTake<T>(skip, take, sql);
        }

        public async Task<List<T>> SkipTakeAsync<T>(long skip, long take, string sql, params object[] args)
        {
            return await _dbBase.SkipTakeAsync<T>(skip, take, sql, args);
        }

        public async Task<List<T>> SkipTakeAsync<T>(long skip, long take, Sql sql)
        {
            return await _dbBase.SkipTakeAsync<T>(skip, take, sql);
        }

        public IEnumerable<T> Query<T>(string sql, params object[] args)
        {
            return _dbBase.Query<T>(sql, args);
        }

        public IEnumerable<T> Query<T>(Sql sql)
        {
            return _dbBase.Query<T>(sql);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, params object[] args)
        {
            return await _dbBase.QueryAsync<T>(sql, args);
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(Sql sql)
        {
            return await _dbBase.QueryAsync<T>(sql);
        }

        public IQueryProviderWithIncludes<T> Query<T>()
        {
            return _dbBase.Query<T>();
        }

        public T SingleById<T>(object primaryKey)
        {
            return _dbBase.SingleById<T>(primaryKey);
        }

        public async Task<T> SingleByIdAsync<T>(object primaryKey)
        {
            return await _dbBase.SingleByIdAsync<T>(primaryKey);
        }

        public T SingleOrDefaultById<T>(object primaryKey)
        {
            return _dbBase.SingleOrDefaultById<T>(primaryKey);
        }

        public async Task<T> SingleOrDefaultByIdAsync<T>(object primaryKey)
        {
            return await _dbBase.SingleOrDefaultByIdAsync<T>(primaryKey);
        }

        public T Single<T>(string sql, params object[] args)
        {
            return _dbBase.Single<T>(sql, args);
        }
        public T SingleInto<T>(T instance, string sql, params object[] args)
        {
            return _dbBase.SingleInto(instance, sql, args);
        }

        public T SingleOrDefault<T>(string sql, params object[] args)
        {
            return _dbBase.SingleOrDefault<T>(sql, args);
        }
        public T SingleOrDefaultInto<T>(T instance, string sql, params object[] args)
        {
            return _dbBase.SingleOrDefaultInto(instance, sql, args);
        }

        public T First<T>(string sql, params object[] args)
        {
            return _dbBase.First<T>(sql, args);
        }


        public T FirstInto<T>(T instance, string sql, params object[] args)
        {
            return _dbBase.FirstInto(instance, sql, args);
        }

        public T FirstOrDefault<T>(string sql, params object[] args)
        {
            return _dbBase.FirstOrDefault<T>(sql, args);
        }

        public T FirstOrDefaultInto<T>(T instance, string sql, params object[] args)
        {
            return _dbBase.FirstOrDefaultInto(instance, sql, args);
        }

        public T Single<T>(Sql sql)
        {
            return _dbBase.Single<T>(sql);
        }
        public T SingleInto<T>(T instance, Sql sql)
        {
            return _dbBase.SingleInto(instance, sql);
        }


        public T SingleOrDefault<T>(Sql sql)
        {
            return _dbBase.SingleOrDefault<T>(sql);
        }


        public T SingleOrDefaultInto<T>(T instance, Sql sql)
        {
            return _dbBase.SingleOrDefaultInto(instance, sql);
        }

        public T First<T>(Sql sql)
        {
            return _dbBase.First<T>(sql);
        }


        public T FirstInto<T>(T instance, Sql sql)
        {
            return _dbBase.FirstInto(instance, sql);
        }


        public T FirstOrDefault<T>(Sql sql)
        {
            return _dbBase.FirstOrDefault<T>(sql);
        }

        public T FirstOrDefaultInto<T>(T instance, Sql sql)
        {
            return _dbBase.FirstOrDefaultInto(instance, sql);
        }


        public Dictionary<TKey, TValue> Dictionary<TKey, TValue>(Sql sql)
        {
            return _dbBase.Dictionary<TKey, TValue>(sql);
        }

        public Dictionary<TKey, TValue> Dictionary<TKey, TValue>(string sql, params object[] args)
        {
            return _dbBase.Dictionary<TKey, TValue>(sql, args);
        }

        public bool Exists<T>(object primaryKey)
        {
            return _dbBase.Exists<T>(primaryKey);
        }

        public TRet FetchMultiple<T1, T2, TRet>(Func<List<T1>, List<T2>, TRet> cb, string sql, params object[] args)
        {
            return _dbBase.FetchMultiple(cb, sql, args);
        }

        public TRet FetchMultiple<T1, T2, T3, TRet>(Func<List<T1>, List<T2>, List<T3>, TRet> cb, string sql, params object[] args)
        {
            return _dbBase.FetchMultiple(cb, sql, args);
        }

        public TRet FetchMultiple<T1, T2, T3, T4, TRet>(Func<List<T1>, List<T2>, List<T3>, List<T4>, TRet> cb, string sql, params object[] args)
        {
            return _dbBase.FetchMultiple(cb, sql, args);
        }

        public TRet FetchMultiple<T1, T2, TRet>(Func<List<T1>, List<T2>, TRet> cb, Sql sql)
        {
            return _dbBase.FetchMultiple(cb, sql);
        }

        public TRet FetchMultiple<T1, T2, T3, TRet>(Func<List<T1>, List<T2>, List<T3>, TRet> cb, Sql sql)
        {
            return _dbBase.FetchMultiple(cb, sql);
        }

        public TRet FetchMultiple<T1, T2, T3, T4, TRet>(Func<List<T1>, List<T2>, List<T3>, List<T4>, TRet> cb, Sql sql)
        {
            return _dbBase.FetchMultiple(cb, sql);
        }

        public Tuple<List<T1>, List<T2>> FetchMultiple<T1, T2>(string sql, params object[] args)
        {
            return _dbBase.FetchMultiple<T1, T2>(sql, args);
        }

        public Tuple<List<T1>, List<T2>, List<T3>> FetchMultiple<T1, T2, T3>(string sql, params object[] args)
        {
            return _dbBase.FetchMultiple<T1, T2, T3>(sql, args);
        }

        public Tuple<List<T1>, List<T2>, List<T3>, List<T4>> FetchMultiple<T1, T2, T3, T4>(string sql, params object[] args)
        {
            return _dbBase.FetchMultiple<T1, T2, T3, T4>(sql, args);
        }

        public Tuple<List<T1>, List<T2>> FetchMultiple<T1, T2>(Sql sql)
        {
            return _dbBase.FetchMultiple<T1, T2>(sql);
        }

        public Tuple<List<T1>, List<T2>, List<T3>> FetchMultiple<T1, T2, T3>(Sql sql)
        {
            return _dbBase.FetchMultiple<T1, T2, T3>(sql);
        }

        public Tuple<List<T1>, List<T2>, List<T3>, List<T4>> FetchMultiple<T1, T2, T3, T4>(Sql sql)
        {
            return _dbBase.FetchMultiple<T1, T2, T3, T4>(sql);
        }

        public IDataParameter CreateParameter()
        {
            return _dbBase.CreateParameter();
        }

        public void AddParameter(DbCommand cmd, object value)
        {
            _dbBase.AddParameter(cmd, value);
        }

        public IDbCommand CreateCommand(DbConnection connection, string sql, params object[] args)
        {
            return _dbBase.CreateCommand(connection, sql, args);
        }

        public ITransaction GetTransaction()
        {
            return _dbBase.GetTransaction();
        }

        public ITransaction GetTransaction(IsolationLevel isolationLevel)
        {
            return _dbBase.GetTransaction(isolationLevel);
        }

        public void SetTransaction(DbTransaction tran)
        {
            _dbBase.SetTransaction(tran);
        }

        public void BeginTransaction()
        {
            _dbBase.BeginTransaction();
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            _dbBase.BeginTransaction(isolationLevel);
        }

        public void AbortTransaction()
        {
            _dbBase.AbortTransaction();
        }

        public void CompleteTransaction()
        {
            _dbBase.CompleteTransaction();
        }

        public object Insert<T>(string tableName, string primaryKeyName, bool autoIncrement, T poco)
        {
            return _dbBase.Insert(tableName, primaryKeyName, autoIncrement, poco);
        }

        public object Insert<T>(string tableName, string primaryKeyName, T poco)
        {
            return _dbBase.Insert(tableName, primaryKeyName, poco);
        }

        public object Insert<T>(T poco)
        {
            return _dbBase.Insert(poco);
        }

        public async Task<object> InsertAsync<T>(T poco)
        {
            return await _dbBase.InsertAsync(poco);
        }

        public void InsertBulk<T>(IEnumerable<T> pocos)
        {
            _dbBase.InsertBulk(pocos);
        }

        public int Update(string tableName, string primaryKeyName, object poco, object primaryKeyValue)
        {
            return _dbBase.Update(tableName, primaryKeyName, poco, primaryKeyValue);
        }

        public int Update(string tableName, string primaryKeyName, object poco)
        {
            return _dbBase.Update(tableName, primaryKeyName, poco);
        }

        public int Update(string tableName, string primaryKeyName, object poco, object primaryKeyValue, IEnumerable<string> columns)
        {
            return _dbBase.Update(tableName, primaryKeyName, poco, primaryKeyValue, columns);
        }

        public int Update(string tableName, string primaryKeyName, object poco, IEnumerable<string> columns)
        {
            return _dbBase.Update(tableName, primaryKeyName, poco, columns);
        }

        public int Update(object poco, IEnumerable<string> columns)
        {
            return _dbBase.Update(poco, columns);
        }

        public int Update(object poco, object primaryKeyValue, IEnumerable<string> columns)
        {
            return _dbBase.Update(poco, primaryKeyValue, columns);
        }

        public int Update(object poco)
        {
            return _dbBase.Update(poco);
        }

        public async Task<int> UpdateAsync(object poco)
        {
            return await _dbBase.UpdateAsync(poco);
        }

        public int Update<T>(T poco, Expression<Func<T, object>> fields)
        {
            return _dbBase.Update(poco, fields);
        }

        public int Update(object poco, object primaryKeyValue)
        {
            return _dbBase.Update(poco, primaryKeyValue);
        }

        public int Update<T>(string sql, params object[] args)
        {
            return _dbBase.Update<T>(sql, args);
        }

        public int Update<T>(Sql sql)
        {
            return _dbBase.Update<T>(sql);
        }

        public IUpdateQueryProvider<T> UpdateMany<T>()
        {
            return _dbBase.UpdateMany<T>();
        }

        public int Delete(string tableName, string primaryKeyName, object poco)
        {
            return _dbBase.Delete(tableName, primaryKeyName, poco);
        }

        public int Delete(string tableName, string primaryKeyName, object poco, object primaryKeyValue)
        {
            return _dbBase.Delete(tableName, primaryKeyName, poco, primaryKeyValue);
        }

        public int Delete(object poco)
        {
            return _dbBase.Delete(poco);
        }

        public async Task<int> DeleteAsync(object poco)
        {
            return await _dbBase.DeleteAsync(poco);
        }

        public int Delete<T>(string sql, params object[] args)
        {
            return _dbBase.Delete<T>(sql, args);
        }

        public int Delete<T>(Sql sql)
        {
            return _dbBase.Delete<T>(sql);
        }

        public int Delete<T>(object pocoOrPrimaryKey)
        {
            return _dbBase.Delete<T>(pocoOrPrimaryKey);
        }

        public IDeleteQueryProvider<T> DeleteMany<T>()
        {
            return _dbBase.DeleteMany<T>();
        }

        public void Save<T>(T poco)
        {
            _dbBase.Save(poco);
        }

        public bool IsNew<T>(T poco)
        {
            return _dbBase.IsNew(poco);
        }
        #endregion 
    }
}
