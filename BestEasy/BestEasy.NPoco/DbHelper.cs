﻿/********************************************************************************
** 作者： Mick
** 邮箱： zheng_jinfan@126.com
** 主页： http://www.zhengjinfan.cn
** 创始时间：2016-2-4
** 修改人：Mick
** 修改时间：2016-2-5
** 描述：
**      数据库帮助类，…
*********************************************************************************/
using System.Configuration;
using System.Data;
using System.Data.Common;
using NPoco;

namespace BestEasy.NPoco
{
    /// <summary>
    /// 数据库帮助类
    /// </summary>
    public class DbHelper
    {
        /// <summary>
        /// Database 实体
        /// </summary>
        public IDatabase Database { get; private set; }

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connection">数据库连接对象</param>
        public DbHelper(DbConnection connection)
        {
            Database = new Database(connection);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connection">数据库连接对象</param>
        /// <param name="dbType">数据库类型</param>
        public DbHelper(DbConnection connection, DatabaseType dbType)
        {
            Database = new Database(connection, dbType);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connection">数据库连接对象</param>
        /// <param name="dbType">数据库类型</param>
        /// <param name="isolationLevel">指定连接的事务锁定行为</param>
        public DbHelper(DbConnection connection, DatabaseType dbType, IsolationLevel? isolationLevel)
        {
            Database = new Database(connection, dbType, isolationLevel);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="providerName">数据库提供程序名称</param>
        public DbHelper(string connectionString, string providerName)
        {
            Database = new Database(connectionString, providerName);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="providerName">数据库提供程序名称</param>
        /// <param name="enableAutoSelect">是否启用自动选择</param>
        public DbHelper(string connectionString, string providerName, bool enableAutoSelect)
        {
            Database = new Database(connectionString, providerName, enableAutoSelect);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="dbType">数据库类型</param>
        public DbHelper(string connectionString, DatabaseType dbType)
        {
            Database = new Database(connectionString, dbType);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="dbType">数据库类型</param>
        /// <param name="isolationLevel">指定连接的事务锁定行为</param>
        public DbHelper(string connectionString, DatabaseType dbType, IsolationLevel? isolationLevel)
        {
            Database = new Database(connectionString, dbType, isolationLevel);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="dbType">数据库类型</param>
        /// <param name="isolationLevel">指定连接的事务锁定行为</param>
        /// <param name="enableAutoSelect">是否启用自动选择</param>
        public DbHelper(string connectionString, DatabaseType dbType, IsolationLevel? isolationLevel, bool enableAutoSelect)
        {
            Database = new Database(connectionString, dbType, isolationLevel, enableAutoSelect);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionStringName">配置文件-数据库连接字符串名称</param>
        public DbHelper(string connectionStringName)
        {
            Database = new Database(connectionStringName);
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="connectionStringName">配置文件-数据库连接字符串名称</param>
        /// <param name="enableAutoSelect">是否启用自动选择</param>
        public DbHelper(string connectionStringName, bool enableAutoSelect)
        {
            Database = new Database(connectionStringName, enableAutoSelect);
        }
        #endregion

        /// <summary>
        /// 当前配置文件配置的数据库 
        /// </summary>
        public static Database CurrentDb
        {
            get
            {
                var dbType = DbTypeHelper.GetDbType;
                switch (dbType)
                {
                    case DbType.MySql:
                        return new Database("MySqlConnectionString");
                    case DbType.SqlServer2008:
                        return new Database("SqlServer2008ConnectionString");
                    default:
                        return new Database("SqlServer2008ConnectionString");
                }
            }
        }
        /// <summary>
        /// 新数据库连接
        /// </summary>
        /// <param name="connectionStringName">配置文件 连接名称</param>
        /// <returns></returns>
        public static Database NewDb(string connectionStringName)
        {
            return new Database(connectionStringName);
        }
    }
    /// <summary>
    /// 数据库类型帮助类
    /// </summary>
    public static class DbTypeHelper
    {
        /// <summary>
        /// 数据库类型
        /// </summary>
        public static DbType GetDbType
        {
            get
            {
                var defatultDb = ConfigurationManager.AppSettings["DefaultDb"];
                switch (defatultDb)
                {
                    case "MySql":
                        return DbType.MySql;
                    case "SqlServer2008":
                        return DbType.SqlServer2008;
                    case "Oracle":
                        return DbType.Oracle;
                    case "OracleManaged":
                        return DbType.OracleManaged;
                    case "PostgreSql":
                        return DbType.PostgreSql;
                    case "SqlCe":
                        return DbType.SqlCe;
                    case "SqLite":
                        return DbType.SqLite;
                    case "SqlServer2005":
                        return DbType.SqlServer2005;
                    case "SqlServer2012":
                        return DbType.SqlServer2012;
                    default:
                        return DbType.SqlServer2008;

                }
            }
        }
    }
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DbType
    {
        MySql,
        SqlServer2005,
        SqlServer2008,
        SqlServer2012,
        Oracle,
        OracleManaged,
        PostgreSql,
        SqlCe,
        SqLite
    }
}
