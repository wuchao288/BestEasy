﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BestEasy.Core.Data;
using BestEasy.Core.Data.NPoco;

namespace BestEasy.CMS.Models.Security
{
    public interface IRoleRepository:IRepository<Role,long>,INPocoRepository<Role,long>
    {
    }
}
