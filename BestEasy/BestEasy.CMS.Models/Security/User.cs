﻿using System;
using BestEasy.Core;
using NPoco;
using BestEasy.Core.Extensions;

namespace BestEasy.CMS.Models.Security
{
    /// <summary>
    /// 用户实体
    /// </summary>
    [TableName("tb_Sec_Users")]
    [PrimaryKey("Id")]
    [Serializable]
    public class User : EntityBase<long>
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set;}
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 是否系统用户
        /// </summary>
        public bool IsSystemUser { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public long RoleId { get; set; }
        /// <summary>
        /// 角色实体属性
        /// </summary>
        [ResultColumn]
        public Role Role { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public bool IsDel { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnable { get; set; }
        /// <summary>
        /// 最后登录IP
        /// </summary>
        public string LastLoginIp { get; set; }
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime LastLoginTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        //验证
        protected override void Validate()
        {
            if (!LastLoginIp.IsIp())
                AddBrokenRule(new BusinessRule("LastLoginIp", "错误信息：IP地址格式有误！"));
        }
    }
}
