﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestEasy.Core.Configuration
{
    public interface IApplicationSettings
    {
        string LoggerName { get; }
        string NumberOfResultsPerPage { get; }
        string JanrainApiKey { get;  }

        string PayPalBusinessEmail { get; }
        string PayPalPaymentPostToUrl { get; }

    }
}
