﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BestEasy.Core.Configuration
{
    public class WebConfigApplicationSettings : IApplicationSettings
    {
        public string LoggerName => ConfigurationManager.AppSettings["LoggerName"];

        public string NumberOfResultsPerPage => ConfigurationManager.AppSettings["NumberOfResultsPerPage"];

        public string JanrainApiKey => ConfigurationManager.AppSettings["JanrainApiKey"];

        public string PayPalBusinessEmail => ConfigurationManager.AppSettings["PayPalBusinessEmail"];

        public string PayPalPaymentPostToUrl => ConfigurationManager.AppSettings["PayPalPaymentPostToUrl"];
    }

}
