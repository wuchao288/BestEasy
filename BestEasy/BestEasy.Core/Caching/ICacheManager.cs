﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Caching
{
    public interface ICacheManager
    {
        /// <summary>
        /// 获取或设置与指定的键相关联的值。
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="key">键</param>
        /// <returns>与指定的键关联的值。</returns>
        T Get<T>(string key);

        /// <summary>
        /// 将指定的键和对象添加到缓存中。
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="data">数据</param>
        /// <param name="cacheTime">缓存时间</param>
        void Set(string key, object data, int cacheTime);

        /// <summary>
        /// 获取一个值，指示是否与指定键关联的值缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>结果</returns>
        bool IsSet(string key);

        /// <summary>
        /// 从缓存中移除指定的键的值
        /// </summary>
        /// <param name="key">键</param>
        void Remove(string key);

        /// <summary>
        /// 通过模式删除项目
        /// </summary>
        /// <param name="pattern">pattern</param>
        void RemoveByPattern(string pattern);

        /// <summary>
        /// 清除所有缓存数据
        /// </summary>
        void Clear();
    }
}
