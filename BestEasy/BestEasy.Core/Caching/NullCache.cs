﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Caching
{
    public class NullCache : ICacheManager
    {
        public void Clear()
        {
            
        }

        public T Get<T>(string key)
        {
            return default(T);
        }

        public bool IsSet(string key)
        {
            return false;
        }

        public void Remove(string key)
        {
            
        }

        public void RemoveByPattern(string pattern)
        {
            
        }

        public void Set(string key, object data, int cacheTime)
        {
            
        }
    }
}
