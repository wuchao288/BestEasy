﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core
{
    /// <summary>
    /// 实体基类
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public abstract class EfEntityBase:IAggregateRoot
    {
        /// <summary>
        /// 获取或设置该实体标识符
        /// </summary>
        public long Id { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as EfEntityBase);
        }

        private static bool IsTransient(EfEntityBase obj)
        {
            return obj != null && Equals(obj.Id, default(int));
        }

        private Type GetUnproxiedType()
        {
            return GetType();
        }

        public virtual bool Equals(EfEntityBase other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) &&
                !IsTransient(other) &&
                Equals(Id, other.Id))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) ||
                        otherType.IsAssignableFrom(thisType);
            }
            return false;
        }

        public override int GetHashCode()
        {
            if (Equals(Id, default(int)))
                return base.GetHashCode();
            return Id.GetHashCode();
        }

        public static bool operator ==(EfEntityBase x, EfEntityBase y)
        {
            return Equals(x, y);
        }

        public static bool operator !=(EfEntityBase x, EfEntityBase y)
        {
            return !(x == y);
        }
    }
}
