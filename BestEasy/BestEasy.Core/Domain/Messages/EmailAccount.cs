﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Domain.Messages
{
    public class EmailAccount : EntityBase<long>
    {
        /// <summary>
        /// 获取或设置一个电子邮件地址
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 获取或设置一个电子邮件显示名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 获取或设置电子邮件主机
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 获取或设置一个电子邮件端口
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 获取或设置一个电子邮件用户名
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// 获取或设置电子邮件密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 获取或设置一个控件是否SmtpClient使用安全套接字层（SSL）值加密连接
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// 获取或设置一个值，该值控制应用程序的默认系统凭据是否发送请求。
        /// </summary>
        public bool UseDefaultCredentials { get; set; }

        /// <summary>
        /// 获取一个友好的电子邮件帐户名
        /// </summary>
        public string FriendlyName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.DisplayName))
                    return this.Email + " (" + this.DisplayName + ")";
                return this.Email;
            }
        }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
