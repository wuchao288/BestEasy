﻿using System.Collections.Generic;

namespace BestEasy.Core.Events
{
    /// <summary>
    /// 领域事件处理程序工厂接口
    /// </summary>
    public interface IDomainEventHandlerFactory
    {
        /// <summary>
        /// 获取领域事件处理程序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param> 
        /// <returns></returns>
        IEnumerable<IDomainEventHandler<T>> GetDomainEventHandlersFor<T>(T domainEvent) where T : IDomainEvent;
    }

}
