﻿using System;

namespace BestEasy.Core.Events
{
    public interface IDomainEventHandler<T>  where T : IDomainEvent
    {
        /// <summary>
        /// 处理
        /// </summary>
        /// <param name="domainEvent"></param>
        void Handle(T domainEvent);
    }

}
