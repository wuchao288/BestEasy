﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Data
{
    /// <summary>
    /// 基础资源库接口
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public interface IRepository<T, in TId> : IReadOnlyRepository<T, TId> where T : IAggregateRoot
    {
        /// <summary>
        /// 新增一条数据
        /// </summary>
        /// <param name="entity"></param>
        void Insert(T entity);
        /// <summary>
        /// 新增多条数据
        /// </summary>
        /// <param name="entitys"></param>
        void InsertBulk(IEnumerable<T> entitys);
        /// <summary>
        /// 修改一条数据
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);
        /// <summary>
        /// 修改多条数据
        /// </summary>
        /// <param name="entitys"></param>
        void UpdateBulk(IEnumerable<T> entitys);
        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(T entity);
        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        int Delete(TId id);
    }
}
