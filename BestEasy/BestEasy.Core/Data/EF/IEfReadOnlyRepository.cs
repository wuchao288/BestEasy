﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Data.EF
{
    /// <summary>
    /// Repository for EF
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEfRepository<T> where T : IAggregateRoot
    {
        IQueryable<T> Table { get; }

        IQueryable<T> TableNoTracking { get; }
    }
}
