﻿using NPoco.Linq;
using System.Threading.Tasks;

namespace BestEasy.Core.Data.NPoco
{
    /// <summary>
    /// 基于 NPoco 的只读资源库接口
    /// </summary>
    /// <typeparam name="T">实例类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public interface INPocoRepository<T,in TId> where T : IAggregateRoot
    {
        /// <summary>
        /// [异步]新增数据
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <returns>主键ID</returns>
        Task<object> InsertAsync(T entity);
        /// <summary>
        /// [异步]更新数据
        /// </summary>
        /// <param name="poco">实体对象</param>
        /// <returns>受影响行数</returns>
        Task<int> UpdateAsync(object poco);
        IUpdateQueryProvider<T> UpdateMany();
        /// <summary>
        /// [异步]删除数据
        /// </summary>
        /// <param name="poco">实体对象</param>
        /// <returns>受影响行数</returns>
        Task<int> DeleteAsync(object poco);
        IDeleteQueryProvider<T> DeleteMany();

        bool Exists(TId id);
        bool IsNew(T entity);
        /// <summary>
        /// 查询对象
        /// </summary>
        /// <returns></returns>
        IQueryProviderWithIncludes<T> Query();
    }
}
