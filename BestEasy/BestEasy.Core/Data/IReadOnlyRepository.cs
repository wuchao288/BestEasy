﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BestEasy.Core.Data
{
    /// <summary>
    /// 基础只读资源库接口
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public interface IReadOnlyRepository<out T, in TId> where T : IAggregateRoot
    {
        /// <summary>
        /// 根据主键ID获取一个实体
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>实体</returns>
        T GetById(TId id);
    }
}
