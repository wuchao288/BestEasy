﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BestEasy.Core.Logging;
using BestEasy.Core.Domain.Messages;

namespace BestEasy.Core.Email
{
    public class TextLoggingEmailService : IEmailService
    {
        public void SendEmail(EmailAccount emailAccount, string subject, string body, string fromAddress, string fromName, string toAddress, string toName, string replyToAddress = null, string replyToName = null, IEnumerable<string> bcc = null, IEnumerable<string> cc = null, string attachmentFilePath = null, string attachmentFileName = null)
        {
            StringBuilder email = new StringBuilder();

            email.AppendLine(string.Format("To: {0}", toAddress));
            email.AppendLine(string.Format("From: {0}", fromAddress));
            email.AppendLine(string.Format("Subject: {0}", subject));
            email.AppendLine(string.Format("Body: {0}", body));

            LoggingFactory.GetLogger().Info(email.ToString());
        }        
    }

}
