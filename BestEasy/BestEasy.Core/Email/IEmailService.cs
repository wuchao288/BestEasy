﻿using BestEasy.Core.Domain.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BestEasy.Core.Email
{
    /// <summary>
    /// 电子邮箱服务接口
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// 发送电子邮件
        /// </summary>
        /// <param name="emailAccount">电子邮件帐户</param>
        /// <param name="subject">主题</param>
        /// <param name="body">主体内容</param>
        /// <param name="fromAddress">发送人地址</param>
        /// <param name="fromName">发送人显示名称</param>
        /// <param name="toAddress">接收人地址</param>
        /// <param name="toName">接收人显示名称</param>
        /// <param name="replyToAddress">回复地址</param>
        /// <param name="replyToName">回复显示名称</param>
        /// <param name="bcc">BCC地址列表</param>
        /// <param name="cc">CC地址列表</param>
        /// <param name="attachmentFilePath">附件文件路径</param>
        /// <param name="attachmentFileName">附件文件名。如果指定，则此文件名将被发送给收件人。否则，“AttachmentFilePath”名称将被使用。</param>
        void SendEmail(EmailAccount emailAccount, string subject, string body, string fromAddress,
            string fromName, string toAddress, string toName, string replyToAddress = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null);
    }
}
