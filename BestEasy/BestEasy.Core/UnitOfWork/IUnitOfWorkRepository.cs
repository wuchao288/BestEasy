﻿namespace BestEasy.Core.UnitOfWork
{
    /// <summary>
    /// 工作单元资源库接口
    /// </summary>
    public interface IUnitOfWorkRepository
    {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="entity"></param>
        void PersistCreationOf(IAggregateRoot entity);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        void PersistUpdateOf(IAggregateRoot entity);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity"></param>
        void PersistDeletionOf(IAggregateRoot entity);
    }
}
