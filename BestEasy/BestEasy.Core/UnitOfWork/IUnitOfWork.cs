﻿namespace BestEasy.Core.UnitOfWork
{
    /// <summary>
    /// 工作单元接口
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// 注册修改
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="unitofWorkRepository">资源库</param>
        void RegisterUpdate(IAggregateRoot entity, IUnitOfWorkRepository unitOfWorkRepository);
        /// <summary>
        /// 注册新增
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="unitofWorkRepository">资源库</param>
        void RegisterAdd(IAggregateRoot entity, IUnitOfWorkRepository unitOfWorkRepository);
        /// <summary>
        /// 注册删除
        /// </summary>
        /// <param name="entity">实体</param>
        /// <param name="unitofWorkRepository">资源库</param>
        void RegisterDelete(IAggregateRoot entity, IUnitOfWorkRepository unitOfWorkRepository);
        /// <summary>
        /// 提交修改
        /// </summary>
        void Commit();
    }
}
