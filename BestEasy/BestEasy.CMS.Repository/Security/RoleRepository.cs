﻿using BestEasy.Data.NPoco;
using BestEasy.CMS.Models.Security;
using BestEasy.Core.UnitOfWork;

namespace BestEasy.CMS.Repository.Security
{
    public class RoleRepository : NPocoRepositoryBase<Role, long>, IRoleRepository
    {
        public RoleRepository(IUnitOfWork unitOfwork)
            :base(unitOfwork)
        {

        }
    }
}
