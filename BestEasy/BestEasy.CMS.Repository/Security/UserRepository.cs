﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BestEasy.Data.NPoco;
using BestEasy.CMS.Models.Security;
using BestEasy.NPoco;
using BestEasy.Core.UnitOfWork;

namespace BestEasy.CMS.Repository.Security
{
    public class UserRepository : NPocoRepositoryBase<User, long>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfwork) :
            base(unitOfwork)
        {

        }
    }
}
